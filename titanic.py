import numpy as np
import pandas as pd
from math import exp, log
from random import randint
from sklearn.preprocessing import normalize

import matplotlib.pyplot as plt

# Import the data
train_data = pd.read_csv('train.csv', sep=',',header=0)
num_data = train_data.shape[0]

features = ['Bias','Age','Male','UpperClass','MiddleClass','LowClass','SibSp','Parch']

# Initialize the parameters to all ones
parameters = np.ones(len(features))

# Hyperparameter: leaning rate
alpha = 0.01
maxIterations = 2000

def prepare_data(input_data):
	# Barely any Cabin data, drop the whole column
	input_data.drop(["Cabin"], axis=1, inplace=True)

	# Drop any rows with null values in them (Age is the limiting row with 714 rows. 712 rows remain.)
	input_data.dropna(inplace=True)

	# Create custom features
	input_data['Bias'] = 1.0
	input_data['Male'] = np.where(input_data['Sex']=='male', 1, 0)

	input_data['UpperClass'] = np.where(input_data['Pclass']==1, 1, 0)
	input_data['MiddleClass'] = np.where(input_data['Pclass']==2, 1, 0)
	input_data['LowClass'] = np.where(input_data['Pclass']==3, 1, 0)

	# Return the updated data and its new length
	return input_data, input_data.shape[0]

def scale_features(input_data):
	# Only scale the columns our algorithm will be learning from
	cols = ['Age']

	# Record what the min and max values were, so the data can be restored
	minimums = input_data[cols].min()
	maximums = input_data[cols].max()

	# Perform min-max scaling
	normalized_train_data = input_data.copy()
	normalized_train_data[cols] = (input_data[cols]-input_data[cols].min())/(input_data[cols].max()-input_data[cols].min())
	
	return normalized_train_data, minimums, maximums

def normalize_features(input_data):

	# Only scale the columns our algorithm will be learning from
	cols = ['Age','SibSp','Parch']

	# Calculate the L2 norm for each column
	l2norm = np.sqrt(np.square(input_data[cols]).sum())

	# Normalize the data
	normalized_train_data = input_data.copy()
	normalized_train_data[cols] = normalized_train_data[cols].divide(l2norm)
	return normalized_train_data

def hypothesis(data, theta):
	T_x = theta_x(data, theta)
	return 1 / (1 + np.exp(-T_x))

def theta_x(data, theta):
	global features
	return np.dot(data.loc[:,features], theta)

def update_parameters(parameters):
	global normalized_data, num_data, features

	# Select a random row from the training data (stochastic gradient descent)
	random_x = normalized_data.iloc[randint(0,num_data-1)]
	
	# Update the parameters
	targetDifference = alpha * (random_x['Survived'].astype(np.float64) - random_x['h'].astype(np.float64))
	parameters += targetDifference * random_x.loc[features].values.astype(np.float64)
	return parameters


def plot_learning_convergence(plot_data):

	# Identify which passengers survived
	survived = train_data[train_data['Survived'] == 1]
	dead = train_data[train_data['Survived'] == 0]

	plt.subplot(2,1,1)
	# Scale the data back to its real values before plotting
	plt.plot(survived['Age']*(maximums['Age']-minimums['Age']) + minimums['Age'], 
		survived['Male']-0.02, 'gs', markerfacecolor=(0,1,0,0.1), markeredgecolor='none')
	plt.plot(dead['Age']*(maximums['Age']-minimums['Age']) + minimums['Age'], 
		dead['Male']+0.02, 'ro', markerfacecolor=(1,0,0,0.1), markeredgecolor='none')

	plt.xlabel('Age')
	plt.ylabel('Person is Male')

	# Calculate line of best fit
	x2l = (log(1) - parameters[0])/parameters[2]
	x2r = (log(1) - parameters[0] - parameters[1])/parameters[2]

	plt.plot(np.array([0,1])*(maximums['Age']-minimums['Age']) + minimums['Age'], 
		np.array([x2l, x2r]),'b-')

	plt.legend(['Survived', 'Died', 'Survival Boundary'])
	plt.ylim([-0.5, 1.5])

	plt.subplot(2,1,2)
	# Plot the target
	plt.plot(plot_data['iteration'],plot_data['goal_data'], 'g-')

	# Plot how well our model matched the target
	plt.plot(plot_data['iteration'],plot_data['learn_data'], 'r-')

	plt.xlabel('Iterations')
	plt.ylabel('Number of Correct Predictions')
	plt.legend(['Goal', 'Logistic Regression'])
	plt.ylim([0,750])
	plt.show()


def prepare_test_data(input_data):
	num_data = input_data.shape[0]

	# Barely any Cabin data, drop the whole column
	input_data.drop(["Cabin"], axis=1, inplace=True)
	# Drop the Fare data, we don't use it anyway and it's missing a row
	input_data.drop(["Fare"], axis=1, inplace=True)

	# Can't drop rows, as the Kaggle online submission requires all rows
	# Extract the null rows for side analysis
	ageless = input_data[input_data['Age'].isnull()].copy()

	# We can't learn on this dataset, as it is the test set. Lets just say if the gender is male, they die
	ageless['Survived'] = np.where(ageless['Sex'] == 'female', 1,0)

	# Drop any rows with null values in them 
	input_data.dropna(inplace=True)

	# Create custom features
	input_data['Bias'] = 1.0
	input_data['Male'] = np.where(input_data['Sex']=='male', 1, 0)

	input_data['UpperClass'] = np.where(input_data['Pclass']==1, 1, 0)
	input_data['MiddleClass'] = np.where(input_data['Pclass']==2, 1, 0)
	input_data['LowClass'] = np.where(input_data['Pclass']==3, 1, 0)

	# Return the updated data and its new length
	return input_data, input_data.shape[0], ageless


def output_submission(test_data, ageless):
	global parameters

	# Predict the test target using the current parameters
	hyp = hypothesis(test_data, parameters)

	test_data['Survived'] = np.where(hyp>=0.5, 1, 0)

	cols = ['PassengerId','Survived']
	submission = test_data[cols].append(ageless[cols])

	# Output to CSV file for upload to Kaggle
	submission.to_csv('implementation_submission.csv', index=False)

# Prepare the data for putting through the learning models
train_data, num_data = prepare_data(train_data)

# Scale the data between 0 and 1, and remember the previous min and max values
normalized_data,minimums,maximums = scale_features(train_data)

# normalized_data = normalize_features(train_data)

data_to_plot = {'learn_data' : [], 'iteration' : [], 'goal_data': []}

for loop in range(0,maxIterations):
	if loop % (maxIterations/10) == 0:
		print("Loop " + str(loop))

	hyp = hypothesis(normalized_data, parameters)

	normalized_data['h'] = hyp
	normalized_data['Hypothesis'] = np.where(hyp>=0.5, 1, 0)

	# Count how many predictions we got right
	good_predictions = normalized_data[normalized_data['Survived'] == normalized_data['Hypothesis']]['Survived'].count()

	# Apply gradient descent
	parameters = update_parameters(parameters)

	data_to_plot['iteration'].append(loop)
	data_to_plot['learn_data'].append(good_predictions)
	# The goal is that we get every prediction right, so the goal data is the number of training points
	data_to_plot['goal_data'].append(num_data)



# Output how well we did and plot the convergence
print("Learning accuracy " + str(data_to_plot['learn_data'][-1] / data_to_plot['goal_data'][-1]))
plot_learning_convergence(pd.DataFrame(data_to_plot))

# Now apply these parameters to the test set
test_data = pd.read_csv('test.csv', sep=',',header=0)
test_data, num_test_data, ageless = prepare_test_data(test_data)

normalized_test_data = normalize_features(test_data)
output_submission(normalized_test_data, ageless)

