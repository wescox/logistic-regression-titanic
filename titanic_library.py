import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression

# Import the data
train_data = pd.read_csv('train.csv', sep=',',header=0)
num_data = train_data.shape[0]

features = ['Age','Male','UpperClass','MiddleClass','LowClass','SibSp','Parch']

maxIterations = 2000

logreg = LogisticRegression()

def prepare_data(input_data):
	# Barely any Cabin data, drop the whole column
	input_data.drop(["Cabin"], axis=1, inplace=True)

	# Drop any rows with null values in them (Age is the limiting row with 714 rows. 712 rows remain.)
	input_data.dropna(inplace=True)

	# Create custom features
	input_data['Male'] = np.where(input_data['Sex']=='male', 1, 0)

	input_data['UpperClass'] = np.where(input_data['Pclass']==1, 1, 0)
	input_data['MiddleClass'] = np.where(input_data['Pclass']==2, 1, 0)
	input_data['LowClass'] = np.where(input_data['Pclass']==3, 1, 0)

	# Return the updated data and its new length
	return input_data, input_data.shape[0]

def perform_library_logistic_regression():
	global logreg, train_data, features

	y_train = train_data['Survived']
	X_train = train_data[features]

	logreg.max_iter = maxIterations
	logreg.solver = 'sag'
	logreg.fit(X_train, y_train)

	y_pred = logreg.predict(X_train)

	return logreg.score(X_train, y_train)

def prepare_test_data(input_data):
	num_data = input_data.shape[0]

	# Barely any Cabin data, drop the whole column
	input_data.drop(["Cabin"], axis=1, inplace=True)
	# Drop the Fare data, we don't use it anyway and it's missing a row
	input_data.drop(["Fare"], axis=1, inplace=True)

	# Can't drop rows, as the Kaggle online submission requires all rows
	# Extract the null rows for side analysis
	ageless = input_data[input_data['Age'].isnull()].copy()

	# We can't learn on this dataset, as it is the test set. Lets just say if the gender is male, they die
	ageless['Survived'] = np.where(ageless['Sex'] == 'female', 1,0)

	# Drop any rows with null values in them 
	input_data.dropna(inplace=True)

	# Create custom features
	input_data['Male'] = np.where(input_data['Sex']=='male', 1, 0)

	input_data['UpperClass'] = np.where(input_data['Pclass']==1, 1, 0)
	input_data['MiddleClass'] = np.where(input_data['Pclass']==2, 1, 0)
	input_data['LowClass'] = np.where(input_data['Pclass']==3, 1, 0)

	# Return the updated data and its new length
	return input_data, input_data.shape[0], ageless


def output_submission(test_data, ageless):
	global logreg

	y_pred = logreg.predict(test_data[features])

	test_data['Survived'] = y_pred

	cols = ['PassengerId','Survived']

	submission = test_data[cols].append(ageless[cols])

	submission.to_csv('implementation_submission_library.csv', index=False)

train_data, num_data = prepare_data(train_data)

learning_accuracy = perform_library_logistic_regression()

print("Learning accuracy " + str(learning_accuracy))

# Now apply these parameters to the test set
test_data = pd.read_csv('test.csv', sep=',',header=0)
test_data, num_test_data, ageless = prepare_test_data(test_data)

output_submission(test_data, ageless)
